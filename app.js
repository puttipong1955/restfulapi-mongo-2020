const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
require("dotenv/config");
// Medel
const User = require("./model/user");

const cutomMiddleware = (req, res, next) => {
  console.log("Welcome to my middlware");
  next();
};

app.use(cutomMiddleware);
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.send("First request");
});

app.get("/users", (req, res) => {
  let users = ["Pawan", "SuperGuy", "Flutter Guy", "Hax"];
  res.send({ users: users });
});

app.post("/create-user", async (req, res) => {
  try {
    const newUser = new User(req.body);
    await newUser.save();
    res.send(newUser);
  } catch (error) {
    res.send({ message: error });
  }
});

mongoose.connect(
  process.env.DB_CONNECTION_STRING,
  { useUnifiedTopology: true, useNewUrlParser: true },
  (req, res) => {
    console.log("Connected to database");
  }
);

app.listen(3000, () => {
  console.log("Listening to 3000");
});
